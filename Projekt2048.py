import pygame
import random
 
pygame.init()
pygame.font.init()

WIDTH = 1600
HEIGHT = 900
RED = [ 255, 0, 0 ]
BLUE = [ 0, 0, 255 ]
myfont= pygame.font.SysFont('Arial',40)

velicina = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(velicina)
#definiranje naziva prozora
pygame.display.set_caption("2048")
size = 4
game = [[0 for x in range(size)] for y in range(size)] 

#------------------------------------------------------------------------------
def onLeftArrowClicked(matrix):
  def moveToLeft(): 
    done = False
    while not done:
      done = True
      for x in range(size):
        for y in range(size):
          if(y != size - 1 and matrix[x][y] == 0 and matrix[x][y + 1] != 0):
            matrix[x][y] = matrix[x][y + 1]
            matrix[x][y + 1] = 0
            done = False
  moveToLeft()
  #add neighbours to the left
  for x in range(size):
    for y in range(size):
      if(y != size - 1 and matrix[x][y] == matrix[x][y + 1]):
        matrix[x][y] += matrix[x][y + 1]
        matrix[x][y + 1] = 0
  moveToLeft()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def onRightArrowClicked(matrix):
  def moveToRight(): 
    done = False
    while not done:
      done = True
      for x in range(size):
        for y in range(size - 1, -1, -1):
          if(y != 0 and matrix[x][y] == 0 and matrix[x][y - 1] != 0):
            matrix[x][y] = matrix[x][y - 1]
            matrix[x][y - 1] = 0
            done = False
  moveToRight()
  #add neighbours to the right
  for x in range(size):
    for y in range(size - 1, -1, -1):
      if(y != 0 and matrix[x][y] == matrix[x][y - 1]):
        matrix[x][y] += matrix[x][y - 1]
        matrix[x][y - 1] = 0
  moveToRight()
#-------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def onUpArrowClicked(matrix):
  def moveUp(): 
    done = False
    while not done:
      done = True
      for x in range(size):
        for y in range(size):
          if(x != size - 1 and matrix[x][y] == 0 and matrix[x + 1][y] != 0):
            matrix[x][y] = matrix[x + 1][y]
            matrix[x + 1][y] = 0
            done = False
  moveUp()
  #add neighbours to up
  for x in range(size):
    for y in range(size):
      if(x != size - 1 and matrix[x][y] == matrix[x + 1][y]):
        matrix[x][y] += matrix[x + 1][y]
        matrix[x + 1][y] = 0
  moveUp()
#-------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def onDownArrowClicked(matrix):
  def moveDown(): 
    done = False
    while not done:
      done = True
      for x in range(size - 1, -1, -1):
        for y in range(size):
          if(x != 0 and matrix[x][y] == 0 and matrix[x - 1][y] != 0):
            matrix[x][y] = matrix[x - 1][y]
            matrix[x - 1][y] = 0
            done = False
  moveDown()
  #add neighbours to down
  for x in range(size - 1, -1, -1):
    for y in range(size):
      if(x != 0 and matrix[x][y] == matrix[x - 1][y]):
        matrix[x][y] += matrix[x - 1][y]
        matrix[x - 1][y] = 0
  moveDown()
#-------------------------------------------------------------------------------

def printMatrix(matrix):
    screen.fill(BLUE)
    for x in range(size):
        for y in range(size):
            start_text = myfont.render("%d" %matrix[y][x],False, RED)
            screen.blit(start_text,(30+100*x,30+100*y))
            print(matrix[x][y]),
        print("")
    print("")

def addRandomNumber(matrix):
    done = False
    hasZeros = False
    for x in range(size):
        for y in range(size):
            if(matrix[x][y] == 0):
                hasZeros = True
    if(not hasZeros):
        return True
    while not done:
        x = random.randint(0,size - 1)
        y = random.randint(0, size - 1)
        number = random.randint(1, 2)
        if(matrix[x][y] == 0):
            if(number == 1):
                matrix[x][y] = 2
            else:
                matrix[x][y] = 4
            done = True
    return False

addRandomNumber(game)
addRandomNumber(game)
addRandomNumber(game)
addRandomNumber(game)

printMatrix(game)

done = False
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                onUpArrowClicked(game)
            if event.key == pygame.K_DOWN:
                onDownArrowClicked(game)
            if event.key == pygame.K_RIGHT:
                onRightArrowClicked(game)
            if event.key == pygame.K_LEFT:
                onLeftArrowClicked(game)
            done = addRandomNumber(game)
            printMatrix(game)
    pygame.display.flip()

print("GAMeE OVER!!!!!")

pygame.quit()