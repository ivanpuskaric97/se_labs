# ukljucivanje biblioteke pygame
import pygame
import random
 
pygame.init()
pygame.font.init()


# definiranje konstanti za velicinu prozora
WIDTH = 1600
HEIGHT = 900
 
RED = [ 255, 0, 0 ]
GREEN = [ 0, 255, 0 ]
BLUE = [ 0, 0, 255 ]
TIRKIZ = [ 64,224,208 ]
MAGENTA = [ 255, 0, 255 ]
YELLOW = [255,255,0]
BLACK = [0,0,0]
 

# tuple velicine prozora
size = (WIDTH, HEIGHT)
 
#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)
#definiranje naziva prozora
pygame.display.set_caption("Nasa kul igra")

bg=pygame.image.load("slika.jpg")
bg=pygame.transform.scale(bg,(WIDTH,HEIGHT))
icon=pygame.image.load("icon.png")
icon=pygame.transform.scale(icon, (100,100))
endscreen=pygame.image.load("bg1.jpg")
endscreen=pygame.transform.scale(endscreen,(WIDTH,HEIGHT))

myfont= pygame.font.SysFont('Arial',40)
scorefont= pygame.font.SysFont('Arial', 72)
timefont= pygame.font.SysFont('Arial',24)
start_text= myfont.render("Press SPACE to play!",False, RED)
end_text=myfont.render("Press SPACE to play again!",False, RED)
easy_text=myfont.render("Difficulty: Easy",False,RED)
medium_text=myfont.render("Difficulty: Medium",False,RED)
hard_text=myfont.render("Difficulty:Hard",False,RED)

state = "start" # "play", "score" 
clock = pygame.time.Clock()
i = 0
duration = 3000
bg_color = TIRKIZ
done = False
easyx=WIDTH/2
easyy=HEIGHT/3
mediumx=WIDTH/2
mediumy=HEIGHT/2
hardx=WIDTH/2
hardy=HEIGHT*0.66
iconx = random.randint(0,WIDTH-100)
icony = random.randint(0,HEIGHT-100)
score = 0
while not done:
    #print (state)
    #print (score)
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if state =="start":
                    state="play"
                    timeleft= duration
                elif state=="score":
                    state="play"
                    duration= 3000
                    timeleft=duration
                    score=0
        if event.type==pygame.MOUSEBUTTONDOWN:
            if state=="start":
                if easypos.collidepoint(event.pos):
                    cof=0.99
                    print(cof)
                if medpos.collidepoint(event.pos):
                    cof=0.95
                    print(cof)
                if hardpos.collidepoint(event.pos):
                    cof=0.9
                    print(cof)
            elif state=="play":        
                if iconpos.collidepoint(event.pos):
                    iconx = random.randint(0,WIDTH-100)
                    icony = random.randint(0,HEIGHT-100)
                    timeleft= duration
                    duration *= cof
                    print(duration)
                    score +=1

            
    #state change
    if state=="play":
        timeleft -=time
        if timeleft <= 0:
            state= "score"

    


    #iscrtavanje
    if state== "start":
        screen.blit(bg,(0,0))
        screen.blit(start_text,(30,30))
        easypos=screen.blit(easy_text,(easyx,easyy))
        medpos=screen.blit(medium_text,(mediumx,mediumy))
        hardpos=screen.blit(hard_text,(hardx,hardy))
    if state=="play":
        screen.fill(BLUE)
        score_text= scorefont.render("Score: %d" %score, False, RED)
        time_text=timefont.render("Time: %2f " %(timeleft/1000) ,False,GREEN)
        time_height=time_text.get_height()
        score_width = score_text.get_width()
        iconpos= screen.blit(icon,(iconx,icony))
        screen.blit(score_text,(WIDTH-score_width-10,10))
        screen.blit(time_text,(10,HEIGHT-time_height-10))

    elif state=="score":

        screen.blit(endscreen,(0,0))
        score_text= scorefont.render("Score: %d" %score, False, RED)
        screen.blit(score_text,(WIDTH-score_width-10,10))
        screen.blit(end_text,(30,30))
    
    
    pygame.display.flip()
   
    #ukoliko je potrebno ceka do iscrtavanja
    #iduceg framea kako bi imao 60fpsa
    time = clock.tick(60)
pygame.quit()
